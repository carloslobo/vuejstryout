var one = new Vue({
  el:"#vue-app-one",
  data:{
    title: "Vue title one"
  },
  methods:{

  },
  computed:{
    greet:function () {
      return "welcome from app one";

    }
  }

})
var two = new Vue({
  el:"#vue-app-two",
  data:{
    title: "Vue title two"

  },
  methods:{
    change:function () {
      one.title = "Not one anymore!;)"
    }
  },
  computed:{
    greet:function () {
      return "Welcome from app two";
    }


  }

})

two.title = " changed from outside";
