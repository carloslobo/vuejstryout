Vue.component('greeting',{
  template:'<p>Hey there I am {{name}}. <button @click="changeName()">Change Name</button></p>',
  data:function(){
    return{
      name:"Yoshi",
    }

  },
  methods:{
    changeName:function () {
      this.name= "Mario";
    }
  }

});
var one = new Vue({
  el:"#vue-app-one",
  data:{
    output:"Your fave food"
  },
  methods:{
    readRefs:function () {
      console.log(this.$refs.input.value);
      this.output = this.$refs.input.value;
      console.log(this.$refs.test.innerText);

    }

  }

})

var two = new Vue({
  el:"#vue-app-two"
})
