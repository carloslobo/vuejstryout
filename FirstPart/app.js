new Vue({
  el:'#vue-app1',
  data: {
    name: 'Shawn',
    job:'ninja',
    website:'http://www.thenetninja.co.uk',
    websiteTag:'<a href="http://www.thenetninja.co.uk">The web</a>'
  },
  methods:{
    greet:function(time){
      return "Good " + time + " " + this.name;
    }
  }
});

new Vue({
  el:"#vue-app2",
  data:{
    age: 25,
    x: 0,
    y: 0
  },
  methods:{
    add:function(inc){
      this.age+=inc;
    },
    sub:function(dec){
      this.age-=dec;
    },
    updateXY:function(event){
      console.log(event);
      this.x = event.offsetX;
      this.y = event.offsetY;
    },
    click:function() {
      alert('You clicked me!')
    }
  }

})

new Vue({
  el: "#vue-app3",
  data:{
    name: "",
    age: ""
  },
  methods:{
    logName: function () {
      console.log("typed name");
    },
    logAge: function(){
      console.log("you just typed age");
    }
  }


})

new Vue({
  el:"#vue-app4",
  data:{
    name:'',
    age:20,
    a: 0,
    b: 0
  },
  methods:{
    /*addToA: function () {
      console.log("addToA");
      return this.a + this.age;
    },
    addToB: function () {
      console.log("addToB");
      return this.b + this.age;

    }*/
  },
  computed:{
    addToA: function () {
      console.log("addToA");
      return this.a + this.age;
    },
    addToB: function () {
      console.log("addToB");
      return this.b + this.age;
    }
  }

})

new Vue({
  el: "#vue-app5",
  data: {
      available:false,
      nearby:false
  },
  methods:{

  },
  computed:{
    compClasses: function () {
      return {available: this.available,
        nearby: this.nearby}
    }
  }
})

new Vue({
  el:"#vue-app6",
  data:{
    err: false,
    succ: false
  },
  methods:{

  },
  computed:{

  }


})
new Vue({
  el:"#vue-app7",
  data:{
    name:'jack',
    characters:['Mario', 'Luigi', 'Yoshi', 'Bowser'],
    ninjas:[
      {name:'Rhu',age:25},
      {name:'Yoshi',age:30},
      {name:'Ken', age:21}
      ]
  },
  methods:{

  },
  computed:{

  }

})
