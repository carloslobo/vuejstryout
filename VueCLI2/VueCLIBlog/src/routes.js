import showBlogs from './component/showBlogs.vue'
import addBlog from './component/addBlog.vue'
import singleBlog from './component/singleBlog.vue'

export default [

  {path:'/', component:showBlogs},
  {path:'/add',component:addBlog},
  {path:'/blog/:id', component:singleBlog}
]
